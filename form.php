<!DOCTYPE html>

<style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
  </head>
  <body>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
<head>
    <link rel="stylesheet" href="style.css ">
  </head>
  <body>
    <div id="vhod">
          <?php 
          if (empty($_SESSION['login'])){
          ?>
          <a href="login.php" >Войти</a>
          
          <?php 
          }else { ?><a href="login.php" >Выйти</a><?php } ?>
          
        </div>
<div class="content">
    <h1 ID="da">Форма</h1>
    <form Action="" method = "POST">
      <p>Ваше ФИО:</p>
      <label>
          <br />
          <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio'];?>" />
      </label><br />
      <label><br />
        <input name="email" 
        type="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" />
      </label><br />

      <p>Ваш год рождения:</p>
      <label><br />
        <select name="yob">
              <option value="1990" <?php if($values['yob'] == 1990) {print 'selected';}?>>1990</option>
              <option value="1991" <?php if($values['yob'] == 1991) {print 'selected';}?>>1991</option>
              <option value="1992" <?php if($values['yob'] == 1992) {print 'selected';}?>>1992</option>
              <option value="1993" <?php if($values['yob'] == 1993) {print 'selected';}?>>1993</option>
              <option value="1994" <?php if($values['yob'] == 1994) {print 'selected';}?>>1994</option>
              <option value="1995" <?php if($values['yob'] == 1995) {print 'selected';}?>>1995</option>
              <option value="1996" <?php if($values['yob'] == 1996) {print 'selected';}?>>1996</option>
              <option value="1997" <?php if($values['yob'] == 1997) {print 'selected';}?>>1997</option>
            </select>
      </label><br />

      <p>Пол:</p><br />
      <label><input type="radio" 
        name="radio-pol" value="1" <?php if ($errors['radio-pol']) {print 'class="error"';} ?> <?php if ($values['radio-pol']==1){print 'checked';}?> />
       М</label>
      <label><input type="radio"
        name="radio-pol" value="2" <?php if ($errors['radio-pol']) {print 'class="error"';} ?> <?php if ($values['radio-pol']==2){print 'checked';}?>/>
        Ж</label><br />

        <p>Кол-во конечностей:</p><br />
      <label>
            <input type="radio" name="radio-kon" value="5" <?php if($values['radio-kon'] == 5) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?> />0
          </label>
          <label>
            <input type="radio" name="radio-kon" value="1" <?php if($values['radio-kon'] == 1) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?>/>1
          </label>
          <label>
            <input type="radio" name="radio-kon" value="2" <?php if($values['radio-kon'] == 2) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?>/>2
          </label>
          <label>
            <input type="radio" name="radio-kon" value="3" <?php if($values['radio-kon'] == 3) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?>/>3
          </label>
          <label>
            <input type="radio" name="radio-kon" value="4" <?php if($values['radio-kon'] == 4) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?> />4
          </label>
          <label>
            <input type="radio" name="radio-kon" value="5" <?php if($values['radio-kon'] == 5) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?> />5
          </label>

        <p>  Сверхспособности:</p>
        <label>
            <select name="sp-sp[]" multiple=multiple>
              <option value="1" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
            <?php
              $arr = str_split($values['sp-sp']);
              foreach($arr as $el)
                if ($el == 1)
                  print 'selected';
            ?>
              >Бессмертие</option>
              <option value="2" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
              <?php
              $arr = str_split($values['sp-sp']);
              foreach($arr as $el)
                if ($el == 2)
                  print 'selected';
            ?>
              > Прохождение сквозь стены</option>
              <option value="3" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
              <?php
              $arr = str_split($values['sp-sp']);
              foreach($arr as $el)
                if ($el == 3)
                  print 'selected';
            ?>
              >Левитация</option>
            </select>
          </label>

        <p>Биография:</p>
        <label><br />
        <textarea name="biography" <?php if ($errors['biography']) {print 'class="error"';} ?> > <?php print $values['biography']; ?> </textarea>
        </label><br />

        <p>Даю согласие на обратоку данных</p><br />
        <label><input type="checkbox" checked="checked"
        name="ok" <?php if ($errors['ok']) {print 'class="error"';} ?> />
        Подпись</label><br />

        <input type="submit" value="Отправить" />
    </form>
</div>
<br/>
  </body>
</html>

